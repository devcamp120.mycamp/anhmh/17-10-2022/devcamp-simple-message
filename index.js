//Câu lệnh này tương tự import express from 'express'; Dùng để import thư viện vào project
const express = require("express");

//Khởi tạo app express
const app = express();

//Khai báo cổng của project 
const port = 8000;

//Khai báo API dạng GET "/" sẽ chạy vào đây
//Callback function là một tham số của hàm khác và nó sẽ được thực thi ngay sau khi hàm đấy được gọi
app.get("/", (request, response) => {
    let today = new Date();

    response.status(200).json({
        message: `Hello! Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

//Chạy app express
app.listen(port, () => {
    console.log("App listening on port (Ứng dụng đang chạy trên cổng)" + port);
})